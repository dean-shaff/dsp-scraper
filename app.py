import os
import time
import argparse
import logging
import asyncio

import aiofiles
import aiohttp
from PyPDF2 import PdfFileMerger
from bs4 import BeautifulSoup

__version__ = "1.0.1"

base_url = "http://www.dspguide.com"

headers = {
    "user-agent": ("Mozilla/5.0 "
                   "(X11; Fedora; Linux x86_64; rv:59.0) "
                   "Gecko/20100101 Firefox/59.0")
}

current_dir = os.path.dirname(os.path.abspath(__file__))
download_dir = os.path.join(current_dir, "downloads")
products_dir = os.path.join(current_dir, "products")

for folder in [download_dir, products_dir]:
    if not os.path.exists(folder):
        os.makedirs(folder)

module_logger = logging.getLogger(__name__)


async def merge_pdfs(file_paths, destination_dir=None):

    if destination_dir is None:
        destination_dir = products_dir

    merger = PdfFileMerger()

    for path in file_paths:
        merger.append(path)

    result_file_path = os.path.join(destination_dir, "DSP.pdf")
    merger.write(result_file_path)


async def get_html(session, url):
    async with session.get(url) as resp:
        module_logger.debug(
            "get_html: url: {}, resp.status: {}".format(
                url, resp.status))
        return await resp.text()


async def get_pdf(session, url, destination_dir=None):
    if destination_dir is None:
        destination_dir = download_dir
    html = await get_html(session, url)
    t0 = time.time()
    soup = BeautifulSoup(html, "html.parser")
    pdf_link = soup.find("b").find("a").get("href")
    module_logger.debug(
        "get_pdf: Took {:.3f} seconds to get link to PDF file".format(
            time.time() - t0))
    pdf_file_name = pdf_link.split("/")[-1]
    pdf_file_path = os.path.join(destination_dir, pdf_file_name)
    async with session.get(pdf_link) as resp:
        module_logger.debug(
            "get_pdf: pdf_link: {}, resp.status: {}".format(
                pdf_link, resp.status))
        file_contents = await resp.read()
        async with aiofiles.open(pdf_file_path, "wb") as file:
            await file.write(file_contents)
    return pdf_file_path


async def main(base_url, headers=None, merge=True):
    module_logger.info("Downloading PDF files from {}".format(base_url))
    pdf_url = base_url + "/pdfbook.htm"
    async with aiohttp.ClientSession(headers=headers) as session:
        links = []
        html = await get_html(session, pdf_url)
        t0 = time.time()
        soup = BeautifulSoup(html, 'html.parser')
        right_column = soup.find("div", dict(id="columnLeft"))
        for li in right_column.find("ul").find_all("li"):
            if li.find("ul") is not None:
                relative_link = str(li.find("a").get("href"))
                links.append(
                    base_url + relative_link
                )
        module_logger.debug(
            "main: Took {:.3f} seconds to get links to PDF pages".format(
                time.time() - t0))
        results = await asyncio.ensure_future(
            asyncio.gather(*(get_pdf(session, li) for li in links))
        )
        if merge:
            module_logger.info(
                "Merging downloaded PDF files into a single document")
            asyncio.ensure_future(
                merge_pdfs(results)
            )


def create_parser():

    parser = argparse.ArgumentParser(
        description=("Download PDF files from www.dspguide.com, "
                     "and optionally create a single DSP PDF "
                     "from all chapters")
    )

    parser.add_argument(
        "--verbose",
        "-v",
        dest="verbose",
        action='store_true',
        default=False,
        help="Specify whether not the logging level should be DEBUG"
    )

    parser.add_argument(
        "--no-merge",
        "-nm",
        dest="no_merge",
        action='store_true',
        default=False,
        help="Specify whether to merge downloaded PDF files into a single file"
    )

    return parser


if __name__ == "__main__":
    parsed = create_parser().parse_args()
    level = logging.INFO
    if parsed.verbose:
        level = logging.DEBUG
    verbose_loggers = [
        "chardet",
        "asyncio"
    ]
    logging.basicConfig(level=level)
    for name in verbose_loggers:
        logging.getLogger(name).setLevel(logging.INFO)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(
        main(base_url, headers=headers, merge=not parsed.no_merge)
    )
