# sync_multi_request.py
import argparse

import requests

base_url = "http://www.dspguide.com"


def get_html(session, url):
    resp = session.get(url)
    return resp.text


def main(n_requests):
    session = requests.Session()
    for i in range(n_requests):
        html = get_html(session, base_url)


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--n-requests",
        "-nr",
        dest="n_requests",
        action='store',
        type=int,
        default=10,
        help="Specify the number of requests to make"
    )
    return parser


if __name__ == "__main__":
    parsed = create_parser().parse_args()
    main(parsed.n_requests)
