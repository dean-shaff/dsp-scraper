import time
import asyncio

import aiofiles
import aiohttp

base_url = 'https://stats.nba.com/stats'

HEADERS = {
    "user-agent": ("Mozilla/5.0 "
                   "(X11; Fedora; Linux x86_64; rv:59.0) "
                   "Gecko/20100101 Firefox/59.0"),
    "cache-control": "max-age=0",
    "connection": "keep-alive"
}


async def get_players(session, player_args):
    endpoint = '/commonallplayers'
    params = {'leagueid': '00', 'season': '2016-17', 'isonlycurrentseason': '1'}
    url = f'{base_url}{endpoint}'
    print('Getting all players...')
    # url = "https://stats.nba.com/stats/commonallplayers?leagueid=00&season=2016-17&isonlycurrentseason=1"
    async with session.get(url, params=params) as resp:
        data = await resp.json()
    player_args.extend(
        [(item[0], item[2]) for item in data['resultSets'][0]['rowSet']])


async def get_player(session, throttle, player_id, player_name, **kwargs):
    endpoint = '/commonplayerinfo'
    url = f'{base_url}{endpoint}'
    params = {'playerid': player_id}
    print(f'Getting player {player_name}')
    async with session.get(url, params=params, **kwargs) as resp:
        data = await resp.text()
    async with aiofiles.open(
            f'downloads/{player_name.replace(" ", "_")}.json', 'w') as file:
        await file.write(data)


async def main():
    player_args = []
    session = aiohttp.ClientSession(headers=HEADERS)
    await asyncio.ensure_future(get_players(session, player_args))
    chunk_size = 15
    for i in range(0, len(player_args), chunk_size):
        chunk = player_args[i:i+chunk_size]
        timedout = True
        while timedout:
            try:
                print(f"Getting chunk {i} through {i+chunk_size}")
                await asyncio.ensure_future(asyncio.gather(
                    *[get_player(session, throttle, *player, timeout=2.0)
                      for player in chunk]
                ))
                timedout = False
            except Exception as err:
                print(f"Failing to get player data. Trying again")
                await asyncio.sleep(5.0)
                await session.close()
                session = aiohttp.ClientSession(headers=HEADERS)

    await session.close()

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
