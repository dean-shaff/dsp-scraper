import requests

base_url = 'https://stats.nba.com/stats'

HEADERS = {
    "user-agent": ("Mozilla/5.0 "
                   "(X11; Fedora; Linux x86_64; rv:59.0) "
                   "Gecko/20100101 Firefox/59.0"),
    "cache-control": "max-age=0"
}


def get_players(player_args):
    endpoint = '/commonallplayers'
    params = {'leagueid': '00', 'season': '2016-17', 'isonlycurrentseason': '1'}
    url = f'{base_url}{endpoint}'
    print('Getting all players...')
    resp = requests.get(url, headers=HEADERS, params=params)
    data = resp.json()
    player_args.extend(
        [(item[0], item[2]) for item in data['resultSets'][0]['rowSet']])


def get_player(player_id, player_name):
    endpoint = '/commonplayerinfo'
    params = {'playerid': player_id}
    url = f'{base_url}{endpoint}'
    print(f'Getting player {player_name}')
    resp = requests.get(url, headers=HEADERS, params=params)
    print(resp)
    data = resp.text
    with open(f'downloads/{player_name.replace(" ", "_")}.json', 'w') as file:
        file.write(data)


player_args = []
get_players(player_args)
for args in player_args:
    get_player(*args)
