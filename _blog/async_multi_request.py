# async_multi_request.py
import argparse
import asyncio

import aiohttp

base_url = "http://www.dspguide.com"


async def get_html(session, url):
    async with session.get(url) as resp:
        html = await resp.text()
        return html


async def main(n_requests):
    async with aiohttp.ClientSession() as session:
        req = [get_html(session, base_url) for i in range(n_requests)]
        results = await asyncio.ensure_future(
            asyncio.gather(*req)
        )


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--n-requests",
        "-nr",
        dest="n_requests",
        action='store',
        type=int,
        default=10,
        help="Specify the number of requests to make"
    )
    return parser


if __name__ == "__main__":
    parsed = create_parser().parse_args()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(
        main(parsed.n_requests)
    )
