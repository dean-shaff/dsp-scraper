## DSP book scraper v1.0.0

Little scraper that downloads the digital signal processing book
The Scientist and Engineer's Guide to Digital Signal Processing
by Steven W. Smith, Ph.D.

See http://www.dspguide.com/copyrite.htm for rules and permissible use.

### Installation

```
pipenv install
```

### Usage

```
python app.py
````
