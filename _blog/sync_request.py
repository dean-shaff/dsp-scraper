# sync_request.py
import requests

base_url = "http://www.dspguide.com"


def get_html(session, url):
    resp = session.get(url)
    return resp.text

def main():
    session = requests.Session()
    html = get_html(session, base_url)
    print(html)

main()
