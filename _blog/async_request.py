# async_request.py
import asyncio

import aiohttp

base_url = "http://www.dspguide.com"


async def get_html(session, url):
    async with session.get(url) as resp:
        html = await resp.text()
        return html


async def main():
    async with aiohttp.ClientSession() as session:
        html = await get_html(session, base_url)
        print(html)

loop = asyncio.get_event_loop()
loop.run_until_complete(
    main()
)
